import numpy as np
import scipy.sparse as sps

import time

from pyspark import SparkContext, SparkConf
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import RowMatrix

data_file_path = 'ACTIVSg2000.mtx'
num_rows = 4000
num_cols = 4000
data_shape = (num_rows,num_cols)

xs, ys, vals = [], [], []

with open(data_file_path) as fp:
    for line in fp:

        if line[0] == '%':
            continue

        line_parts = line.split(' ')

        x = int(line_parts[0]) - 1
        y = int(line_parts[1]) - 1
        val = float(line_parts[2])

        xs.append(x)
        ys.append(y)
        vals.append(val)

# create a sparse matrix
row = np.array(xs)
col = np.array(ys)
data = np.array(vals)
sv = sps.csc_matrix((data, (row, col)), shape=data_shape)

sparse_vectors = []

for r in range(num_rows):
    inds = sv[0].nonzero()[1]
    vals = sv[0].toarray().take(inds)
    sparse_cols = dict(zip(inds,vals))

    sparse_vectors.append(Vectors.sparse(num_cols, sparse_cols))

cores = ['1','2','4','8','16','32','64','128','256']
mems = ['256m','512m','1g', '2g', '3g', '4g', '5g', '6g', '7g', '8g']

grid = np.zeros((len(cores),len(mems)))

for i in range(len(cores)):
    core = cores[i]
    for j in range(len(mems)):
        mem = mems[j]

        conf = SparkConf().setAll([
            ('spark.executor.memory', '1g'),
            ('spark.executor.cores', '3')]
        )

        sc = SparkContext(conf=conf,appName="SVD")
        sc.setLogLevel("ERROR")

        rows = sc.parallelize(sparse_vectors)

        mat = RowMatrix(rows)

        # Compute the top 5 singular values and corresponding singular vectors.

        start = time.time()

        #svd = mat.computeSVD(20, computeU=True)
        #pc = mat.computePrincipalComponents(5)
        mat.columnSimilarities(0.1)

        end = time.time()

        #U = svd.U       # The U factor is a RowMatrix.
        #s = svd.s       # The singular values are stored in a local dense vector.
        #V = svd.V       # The V factor is a local dense matrix.
        # $example off$
        #collected = U.rows.collect()
        #print("U factor is:")
        #for vector in collected:
        #    print(vector)
        #print("Singular values are: %s" % s)
        #print("V factor is:\n%s" % V)

        elapsed = end-start

        print("Cores: " + core)
        print("Memory: " + mem)
        print("Elapsed time:")

        print(elapsed)

        sc.stop()

        grid[i,j] = elapsed

print grid
